
#include <iostream>
class Animal
{
public:
    virtual ~Animal() {};
    virtual void Voise() { std::cout << "\n"; };
};
class Chicken : public Animal
{
public:
    void Voise() override { std::cout << "Kokoko" << "\n"; };
};
class Cow : public Animal
{
public:
    void Voise() override { std::cout << "Muuuu" << "\n"; };
};
class Dog : public Animal
{
public:
    void Voise() override { std::cout << "Woof" << "\n"; };
};
int main()
{
    Animal* kek[3] = { new Chicken(), new Cow(), new Dog() };
    for (int i = 0; i < 3; i++)
    {
        kek[i]->Voise();
        delete kek[i]; 
    }
}